
import {Doughnut} from 'react-chartjs-2';


export default function doughnutchart() {
	const data = {
	  labels: [
	    'Income',
	    'Expense'
	],
	datasets: [{
	  data: [50, 50],
	  backgroundColor: [
	  'rgba(75,192,192,0.4)',
	  'red'
	  ],
	  hoverBackgroundColor: [
	  '#FF6384',
	  'red'
	  ]
	}]
	};

	return(

		<div>
		  <Doughnut
		     data={data}
		     width={150}
		     height={150}
		  />
		</div>


		)
}



