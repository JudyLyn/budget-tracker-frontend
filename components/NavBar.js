import { useContext } from 'react'
import { Navbar, Nav, Dropdown, Button, ButtonGroup } from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../UserContext'
import * as FaIcons from 'react-icons/fa'
import './navbar.module.css'
import AddNewRecord from  './AddNewRecord'



export default function NavBar() {
    //access the global user state via the UserContext
    const { user } = useContext(UserContext)

    return (
        <Navbar bg="light" expand="lg">
            
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                

                    {(user.id !== null)
                    ? (user.isActive === true)
                        ?<>
                            <Link href="/">
                                <a className="navbar-brand">BTapp</a>
                            </Link>

                            <Link href="/categories">
                                <a className="nav-link" role="button">Categories</a>
                            </Link>
                            <Link href="/records">
                                <a className="nav-link" role="button">Records</a>
                            </Link>
                            <Link href="/trends">
                                <a className="nav-link" role="button">Trends</a>
                            </Link>
                            <Link href="/breakdown">
                                <a className="nav-link" role="button">Breakdown</a>
                            </Link>
                            <Link href="/logout">
                                <a className="nav-link" role="button">Logout</a>
                            </Link>
                        </>
                        : 
                        <>
                        <Link href="/">
                            <a className="navbar-brand">BTapp</a>
                        </Link>
                            <Link href="/categories">
                                <a className="nav-link" role="button">Categories</a>
                            </Link>
                          
                           
                            <Link href="/records">
                                <a className="nav-link" role="button">Records</a>
                              </Link>
                             



                            <Link href="/trends">
                                <a className="nav-link" role="button">Trends</a>
                            </Link>
                            <Link href="/breakdown">
                                <a className="nav-link" role="button">Breakdown</a>
                            </Link>
                           
                            <Dropdown as={ButtonGroup}>
                             <Button variant="info"><FaIcons.FaUser/></Button>
                              <Dropdown.Toggle split variant="info" id="dropdown-split-basic" />

                              <Dropdown.Menu>
                                <Dropdown.Item href="/logout">Logout</Dropdown.Item>
                              </Dropdown.Menu>
                            </Dropdown>
                        
                        </>
                    : null}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
