import { Button } from 'react-bootstrap'
import Router from 'next/router'

export default function ArchiveButton({categoryId}) {

   
    
    const deleteRecord = (categoryId) => {
        fetch(`http://localhost:4000/api/categories/${categoryId}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Router.reload()
            }else{
                Router.push('/error')
            }
        })
    }

  
        return <Button variant="danger" onClick={()=>deleteRecord(categoryId)}>Delete</Button>
  
}
