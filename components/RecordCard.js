import { useContext } from 'react'
import { Card, Button, Row, Col } from 'react-bootstrap'
import UserContext from '../UserContext'
import DeleteButton from './DeleteButton'
import moment from 'moment'

export default function RecordCard({recordData}){

   const { user } = useContext(UserContext)

   const textColor = (recordData.categoryType === 'Income') ? 'text-info' : 'text-danger';
  

  return(

<Card id="card" className="mt-3 max-w-screen-xl m-0 sm:m-20 bg-white shadow sm:rounded-lg flex justify-center flex-1">
  <Card.Body>

  	<Row>
  		 <Col xs={ 6 }>
	        <Card.Title>{recordData.categoryName}</Card.Title>
	        <Card.Text>
	          {recordData.categoryType}
	        </Card.Text>
	        <Card.Text>
	         {recordData.description}
	        </Card.Text>
	         <Card.Text>
	        { moment(recordData.dateAdded).format("MMMM D, YYYY") }
	        </Card.Text>
  		 </Col>

  		  <Col xs={ 6 } className="text-right">
		        <Card.Text key={ recordData.amount} className={textColor}>
		        	{ recordData.amount.toLocaleString() }
		        </Card.Text>
		        <Card.Text>
		        	{ recordData.currentBalance.toLocaleString() }
		        </Card.Text>
		  

		    <Card.Text key={recordData._id}>
		        <DeleteButton recordId={recordData._id}/>
		    </Card.Text>
	    </Col>
  	</Row>
  </Card.Body>
</Card>


    )
}

