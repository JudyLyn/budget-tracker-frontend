import { useContext } from 'react'
import { Card, Button, Row, Col } from 'react-bootstrap'
import UserContext from '../UserContext'
import DeleteCategory from './DeleteCategory'

export default function RecordCard({categoryItem}){

   const { user } = useContext(UserContext)
  return(

 
<Card id="card" className="mt-3 max-w-screen-xl m-0 sm:m-20 bg-white shadow sm:rounded-lg flex justify-center flex-1">
  <Card.Body>

  <Row>

    <Col xs={8} ms={8} lg={8}>
        <Card.Title>{categoryItem.categoryName}</Card.Title>
        <Card.Text>
          {categoryItem.categoryType}
        </Card.Text>
    </Col>

    <Col xs={8} ms={8} lg={8} className="mt-3">
        <Card.Text key={categoryItem._id}>
            <DeleteCategory categoryId={categoryItem._id}/>
        </Card.Text>
    </Col>

  </Row>
  </Card.Body>
</Card>
)
}