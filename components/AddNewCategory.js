import { useState, useEffect, useContext} from 'react'
import { Form, Row, Col, Jumbotron, Card, Button, CardGroup, Modal, Alert} from 'react-bootstrap'
import UserContext from '../UserContext'
import Router from 'next/router'

export default function AddNewCategory(){

	const { user } = useContext(UserContext)
    const [categoryName, setCategoryName] =useState('')
    const [categoryType, setCategoryType] =useState('')

    const [isActive, setIsActive] =useState('')
    const [notify, setNotify] = useState(false)
    const [activeCategories, setActiveCategories] = useState([])


    useEffect(()=>{
        if(categoryName.length < 50) {
            setIsActive(true)
        }else {
            setIsActive(false)
        }
    }, [categoryName])



	 	function createCategory(e){
	        e.preventDefault()

	        fetch('http://localhost:4000/api/categories/add-category',{
	            method: 'POST',
	            headers: {
	                'Content-Type': 'application/json',
	                'Authorization': `Bearer ${localStorage.getItem('token')}`
	            },
	            body: JSON.stringify({
	                categoryName: categoryName,
	                categoryType: categoryType,
	            })
	        })
	        .then(res => res.json())
	        .then(data => {
	        	console.log(data)
	            if(data===true){
	                Router.reload()
	            }else{
	                setNotify(true)
	            }
	        })
	    }



	return(
<>
				        <Form
				        onSubmit={(e)=> createCategory(e)} 
				        className='w-full flex-1 text-indigo-500 mx-auto p-5'>
				        <h3 className="mt-3">Add New Category</h3>
				                            <Form.Group className="mx-auto max-w-xs relative mt-2">
				                                Category Name:
				                                <Form.Control 
				                                value={categoryName}
				                                onChange={e => setCategoryName(e.target.value)}
				                                required
				                                type="text"  
				                                 className='email rounded-pill'
				                                 placeholder=""/>				                                 
				                            </Form.Group> 

				                            <Form.Group className="mx-auto max-w-xs relative">
				                                Category Type:
				                                <Form.Control 
				                                  value={categoryType}
				                                onChange={e => setCategoryType(e.target.value)}
				                                required
				                                type="text"  
				                                 className='email rounded-pill'
				                                 placeholder=""
				                                as="select"  
				                                 className='email rounded-pill'
				                                 placeholder="">
				                                
				                                <option value="">Select Category Type</option>
				                                <option value="Income">Income</option>
				                                <option value="Expense">Expense</option>
				                                </Form.Control>
				                            </Form.Group>
				                                
				                        <div className='my-12 border-b text-center'>
					                        {isActive===true
					                        ? <Button type="submit" className="rounded-pill" variant="info" block>Add</Button>
					                        : <Button disabled type="submit" className="rounded-pill" variant="info" block>Add</Button>}

				        				</div>
				        </Form>

				                    {notify === true
				                    ? <Alert variant="danger">Failed to create a Category!</Alert>
				                    : null}
			</>


				)
}