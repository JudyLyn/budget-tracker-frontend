import { useState, useEffect, useContext} from 'react'
import { Form, Row, Col, Jumbotron, Card, Button, CardGroup, Modal, Alert} from 'react-bootstrap'
import UserContext from '../../UserContext'
import CategoryCard from '../../components/CategoryCard'
import AddNewCategory from '../../components/AddNewCategory'

export default function categories({ data }){

	const { user } = useContext(UserContext)
    

	return(

		<div>
	      <Row>
	        <Col xs={12} ms={5} lg={5} className="mt-5 pt-5"> 
	        	<div className="mt-5">
				<div className='mx-auto max-w-screen-xl m-0 sm:m-20 bg-white shadow sm:rounded-lg flex justify-center flex-1 mt-1 mb-1' id="card">
				       <AddNewCategory/>
				</div>
				</div>
	        </Col>

	        <Col xs={12} ms={7} lg={7} className="mx-auto mt-4"> 
	            <h1>Categories </h1>
			{(data.length === 0)
                ? (
                	<Jumbotron id="card">
                      <h1>There are no available Categories yet</h1>
                  	</Jumbotron>
                  )
                : 
                (
                    data.map(categoryItem => 
                    	<CategoryCard categoryItem={categoryItem}/>)
					)
	        }
	        </Col>   
	      </Row>
	    </div>


		)
}


export async function getServerSideProps() {
  const res = await fetch('http://localhost:4000/api/categories')
  const data = await res.json()

  return {
    props:{
      data
    }
  }
}