import { useState, useEffect, useContext } from 'react'
import UserContext from '../../UserContext'
import DoughnutChart from '../../components/DoughnutChart'
import { Row, Col } from 'react-bootstrap'

export default function breakdown() {

const { setUser } = useContext(UserContext)

  return (
    
      <div id="card Chart" className="mt-3 max-w-screen-xl m-0 sm:m-20 bg-white shadow sm:rounded-lg flex justify-center flex-1">
      <Row>
        <Col xs={12} ms={4} lg={4}>
        </Col>
        <Col xs={12} ms={4} lg={4}>

          <h3 className="m-5">Income Vs Expense</h3>
          <div id="DoughnutChart">
            <DoughnutChart />
          </div>
     
        </Col>
        <Col xs={12} ms={4} lg={4}>
        </Col>
      </Row>
      </div>
    
  )
}


